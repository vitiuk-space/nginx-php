# docker-ubuntu-base
Docker automated daily build: https://gitlab.com/webakcent/docker/docker-ubuntu-base

Slightly modified phusion/baseimage Ubuntu base image

- Includes `ping` and `tzdata` packages
- Container timezone is set via environment variable `${CONTAINER_TIMEZONE:-$DEFAULT_CONTAINER_TIMEZONE}`, defaults to `Europe/Kiev`
- Modified apt sources.list to use `http://mirror.rackspace.com/ubuntu/`, which seems reliable, fast and local to most servers
