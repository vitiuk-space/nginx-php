#!/bin/bash
set -euf

# =============================================================================
# 	PHP5-FPM
# =============================================================================

# Ubuntu xenial php doesn't create /run/php, where it expects socket files to live
chown -R ${APP_USER:-$DEFAULT_APP_USER} /run/php

# replace PHP Pool name
POOL_NAME=`echo ${APP_HOSTNAME} | sed -e 's/[^a-zA-Z]/_/g'`
echo " * php:    pool name   ${POOL_NAME}"
sed -i -r "s/^\[.*\]$/\[${POOL_NAME}\]/g" /etc/php/5.6/fpm/pool.d/www.conf

# set php-fpm user to match nginx
echo " * php:    user:  ${APP_USER:-$DEFAULT_APP_USER}"
echo " * php:    group: ${APP_GROUP:-$DEFAULT_APP_GROUP}"
sed -i -r "s/^user\s*=.+$/user = ${APP_USER:-$DEFAULT_APP_USER}/g" /etc/php/5.6/fpm/pool.d/www.conf
sed -i -r "s/^group\s*=.+$/group = ${APP_GROUP:-$DEFAULT_APP_GROUP}/g" /etc/php/5.6/fpm/pool.d/www.conf
sed -i -r "s/^listen.owner\s*=.+$/listen.owner = ${APP_USER:-$DEFAULT_APP_USER}/g" /etc/php/5.6/fpm/pool.d/www.conf
sed -i -r "s/^listen.group\s*=.+$/listen.group = ${APP_GROUP:-$DEFAULT_APP_GROUP}/g" /etc/php/5.6/fpm/pool.d/www.conf


# memory limit
echo " * php:    memory_limit:         ${PHP_MEMORY_LIMIT:-$DEFAULT_PHP_MEMORY_LIMIT}"
sed -i -r "s/^.*memory_limit\s*=\s*[0-9]+[MG]/memory_limit = ${PHP_MEMORY_LIMIT:-$DEFAULT_PHP_MEMORY_LIMIT}/g" /etc/php/5.6/fpm/php.ini

# file upload limits
echo " * php:    upload_max_filesize:  ${UPLOAD_MAX_SIZE:-$DEFAULT_UPLOAD_MAX_SIZE}"
echo " * php:    post_max_size:        ${UPLOAD_MAX_SIZE:-$DEFAULT_UPLOAD_MAX_SIZE}"
sed -i -r "s/^.*upload_max_filesize\s*=\s*[0-9]+M/upload_max_filesize = ${UPLOAD_MAX_SIZE:-$DEFAULT_UPLOAD_MAX_SIZE}/g" /etc/php/5.6/fpm/php.ini
sed -i -r "s/^.*post_max_size\s*=\s*[0-9]+M/post_max_size = ${UPLOAD_MAX_SIZE:-$DEFAULT_UPLOAD_MAX_SIZE}/g" /etc/php/5.6/fpm/php.ini

# maximum file uploads
echo " * php:    max_file_uploads:   ${PHP_MAX_FILE_UPLOAD:-$DEFAULT_PHP_MAX_FILE_UPLOAD}"
sed -i -r "s/^.*max_file_uploads\s*=.*$/max_file_uploads = ${PHP_MAX_FILE_UPLOAD:-$DEFAULT_PHP_MAX_FILE_UPLOAD}/g" /etc/php/5.6/fpm/php.ini

# maximum execution time
echo " * php:    max_execution_time:   ${PHP_MAX_EXECUTION_TIME:-$DEFAULT_PHP_MAX_EXECUTION_TIME}"
sed -i -r "s/^.*max_execution_time\s*=.*$/max_execution_time = ${PHP_MAX_EXECUTION_TIME:-$DEFAULT_PHP_MAX_EXECUTION_TIME}/g" /etc/php/5.6/fpm/php.ini

# maximum input variables
echo " * php:    max_input_vars:       ${PHP_MAX_INPUT_VARS:-$DEFAULT_PHP_MAX_INPUT_VARS}"
sed -i -r "s/^.*max_input_vars\s*=.*$/max_input_vars = ${PHP_MAX_INPUT_VARS:-$DEFAULT_PHP_MAX_INPUT_VARS}/g" /etc/php/5.6/fpm/php.ini

# sendmail parameters
echo " * php:    sendmail_path:         /usr/sbin/ssmtp -t"
sed -i -r "s/sendmail_path =.*$/sendmail_path = \/usr\/sbin\/ssmtp -t/g" /etc/php/5.6/fpm/php.ini

# start PHP
exec php-fpm5.6 -c /etc/php/5.6/fpm
