[![build status](https://img.shields.io/badge/php-7.0.33-brightgreen.svg)](https://gitlab.com/webakcent/nginx-php/commits/master)



# Nginx + PHP7-FPM

Настраиваемый сервер nginx-веб-сервер с PHP стек построен на [registry.gitlab.com/webakcent/docker/nginx-pagespeed](https://gitlab.com/webakcent/docker/docker-ubuntu-base), который в свою очередь построен на [слегка изменен Phusion базовый образ Ubuntu](https://hub.docker.com/r/registry.gitlab.com/webakcent/ubuntu/)

Docker Hub: [registry.gitlab.com/webakcent/docker/nginx-php](https://gitlab.com/webakcent/docker/nginx-php/)

`docker run -p "80:80" -p "443:443" -e "APP_HOSTNAME=some.example.com" -v /some/dir/www:/app/www registry.gitlab.com/webakcent/docker/nginx-php`

Настраивается через множество переменных среды, которые применяются при старте службы

var | default | description
--- | ------- | -----------
ADMIN_EMAIL | nobody@example.com | Server administrator email, used for intercepted email in `development` mode
CHOWN_APP_DIR | true | if true, `chown -R $APP_USER:$APP_GROUP /app/www`
APP_HOSTNAME | `hostname -f` |  hostname of application
VIRTUAL_HOST | example.com | virtualhosts which this service should respond to, separated by commmas.  Useful for operating behind [jwilder/nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy/).
TIMEZONE | Australia/Sydney | Server timezone
APP_USER | app | nginx and php7.0-fpm user
APP_GROUP | app | nginx and php7.0-fpm group
APP_UID | 1000 | user_id - useful when mounting volumes from host > guest to either share or delineate file access permission
APP_GID | 1000 | group_id
UPLOAD_MAX_SIZE | 30M | Maximum upload size, applied to nginx and php7.0-fpm
NGINX_MAX_WORKER_PROCESSES | 8 | nginx worker_processes is determined from number of processor cores on service start, up to the maximum permitted by NGINX_MAX_WORKER_PROCESSES
PHP_MEMORY_LIMIT | 128M | Maximum memory PHP can use per worker
PHP_PROCESS_MANAGER | dynamic | dynamic, static, ondemand :: PHP process manager scheme
PHP_MAX_CHILDREN | 6 | process manager maximum spawned children
PHP_START_SERVERS | 3 | if PHP_PROCESS_MANAGER is dynamic, this is the number of children spawned on boot
PHP_MIN_SPARE_SERVERS | 2 | if PHP_PROCESS_MANAGER is dynamic, this is the minimum number of idle children
PHP_MAX_SPARE_SERVERS | 3 | if PHP_PROCESS_MANAGER is dynamic, this is the maximum number of idle children
PHP_MAX_REQUESTS | 500 | Maximum number of requests each child process can process before terminating, which should mitigate any memory leaks. Set to 0 to disable.
PHP_DISABLE_FUNCTIONS | false | Comma separated list of additional functions to disable for security.  These are appended to the default Ubuntu distribution disable_functions line
